This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Given the many ways there are to work with React.js and D3 together (more on this here: [Bringing Together React, D3, And Their Ecosystem](https://www.smashingmagazine.com/2018/02/react-d3-ecosystem/)), I opted for using a library for this case: [nivo](https://nivo.rocks/)

## Installation
In the project directory run:
### `git checkout develop`
and then:
### `npm i`
## Start the App
Before running the app is required to add a value for an environment variable in the **.env** file:

`REACT_APP_API_BASE_URL=https://konuxdata.getsandbox.com`

To run the app in the development mode run:
### `npm start`

This will open [http://localhost:3000](http://localhost:3000) in your browser.

