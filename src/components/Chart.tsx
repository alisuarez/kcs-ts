import * as React from 'react';
import { ResponsiveLine } from '@nivo/line'

const Chart = ({ data }: { data: Array<any> }) =>(
  <ResponsiveLine
    data={data}
    margin={{
      "top": 50,
      "right": 110,
      "bottom": 50,
      "left": 60
    }}
    animate
    xScale={{type: 'time', format: 'native', precision: 'hour' }}
    yScale={{ type: 'linear', stacked: false }}
    axisBottom={{ format: '%b %d' }}
    curve="step"
    enableDotLabel
    dotSize={16}
    dotBorderWidth={1}
    dotBorderColor="inherit:darker(0.3)"
  />
);
export default Chart;