import React, { Fragment } from "react";
import { Button, Form as BSForm, FormGroup, Label, Input } from "reactstrap";
import moment from "moment";
import Datetime from "react-datetime";
import 'react-datetime/css/react-datetime.css'


type FormProps = {
  dateValue?: object,
  inputValue?: number,
  updateValue?: boolean,
  handleInputChange?: (...args: any[]) => any,
  handleDateChange?: (...args: any[]) => any,
  handleSubmit?: (...args: any[]) => any
};
const Form: React.SFC<FormProps> = props => {
  const {
    handleInputChange,
    handleDateChange,
    handleSubmit,
    inputValue,
    updateValue
  } = props;
  const dateValue = moment(props.dateValue);
  const shortcuts = {
    Today: moment(),
    Yesterday: moment().subtract(1, "days"),
    Clear: ""
  };
  return (
    <Fragment>
      <BSForm onSubmit={handleSubmit}>
        <FormGroup>
          <Label for="dateValue">Date</Label>
          <Datetime onChange={handleDateChange}/>
        </FormGroup>
        <FormGroup>
          <Label for="inputValue">Value</Label>
          <Input
            id="inputValue"
            type="number"
            name="inputValue"
            value={inputValue}
            placeholder="Type a value"
            onChange={handleInputChange}
          />
        </FormGroup>
        <FormGroup check>
          <Label check>
            <Input
              name="updateValue"

              type="checkbox"
              onChange={handleInputChange}
            />{" "}
            Update chart with new values
          </Label>
        </FormGroup>
        <Button type="submit">Save</Button>
      </BSForm>
    </Fragment>
  );
};
Form.defaultProps = {
  dateValue: moment(),
  updateValue: false,
  handleInputChange: () => {},
  handleDateChange: () => {},
  handleSubmit: () => {}
};
export default Form;
