import * as React from 'react';
import axios from 'axios';
import moment from 'moment';

import Chart from '../components/Chart';
import Form from '../components/Form';

import { FETCH_POINTS_URL, ADD_POINTS_URL } from '../constants';

interface IState {
  points: Array<any>,
  loaded: boolean,
  updateValue: boolean,
  inputValue: number,
  [key: string]: any
}

export default class Main extends React.Component<{}, IState> {
  state: Readonly<IState> = {
    points: [],
    loaded: false,
    inputValue: 0,
    updateValue: false
  };

  public componentDidMount() {
    this.fetchPoints();
  }

  /**
   * Gets data from the http api and updates the state
   * @returns {boolean} - Whether the request was successful or not
   */
  private async fetchPoints(): Promise<boolean> {
    const options = {
      url: `${FETCH_POINTS_URL}`,
      method: 'get'
    };
    try {
      const response = await axios(options);
      const points = response.data.values;
      this.setState({ points, loaded: true });
      return true;
    } catch (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response);
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the
        // browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
      }
    }
    return false;
  }

  /**
   * Updates state with form values
   * @param {name}
   * @param {value}
   */
  private handleChange = (name: string, value: any) => {
    this.setState({
      [name]: value
    });
  }

  /**
   * Handles input change
   * @param {React.FormEvent<HTMLFormElement>} e - The input event
   */
  private handleInputChange = (e: React.FormEvent<HTMLInputElement>): void => {
    const target = e.currentTarget;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.handleChange(name, value);
  }

  /**
   * Handles date input change
   * @param {value}
   */
  private handleDateChange = (value: Date) => {
    this.handleChange('dateValue', value);
  };

  /**
   * Handles form submission
   * @param {React.FormEvent<HTMLFormElement>} e - The form event
   */
  private handleSubmit = async (
    e: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    e.preventDefault();
    const data = {
      x: this.state.dateValue,
      y: Number(this.state.inputValue)
    };
    const submitSuccess: boolean = await this.submitForm(data);
    if (submitSuccess) {
      //NotificationManager.success('The data was saved', 'Success!', 3000);
    }
  };

  /**
   * Submits the form to the http api
   * @returns {boolean} - Whether the form submission was successful or not
   */
  private async submitForm({ x, y }: { x: Date, y: number }): Promise<boolean> {
    const data = { x, y };
    const options = {
      url: ADD_POINTS_URL,
      method: 'post',
      data
    };
    try {
      const response = await axios(options);
      if (response.data.status === 'ok') {
        if (this.state.updateValue) {
          const { points } = this.state;
          points.push(data);
          this.setState({ points });
        }
      }
      return true;
    } catch {
      return false;
    }
  }

    /**
   * Gets data from the http api and updates the state
   * @returns {Array} - Dates grouped and sorted in ascending order
   */
  getChartData(points: Array<{ x: string, y: number }>): Array<any> {
    points.sort((a, b) => {
      const aDate = new Date(a.x);
      const bDate = new Date(b.x)
      return aDate.getTime() - bDate.getTime();
    });
    points.forEach(function (p) {
      const d = new Date(p.x);
      p.x = d.toString();
    });
    const groupedPoints = [
      ...points.reduce(
        (map, item) => {
          const { x: key, y } = item;
          const prev = map.get(key);
          if (prev) {
            prev.y += y;
          } else {
            map.set(key, Object.assign({}, item));
          }
          return map;
        },
        new Map()
      ).values()
    ];
    groupedPoints.forEach(function (p) {
      p.x = new Date(p.x);
    });
    const data = [
      {
        id: 'Data',
        color: 'hsl(115, 70%, 50%)',
        data: groupedPoints
      }
    ];
    return data;
  }

  render() {
    const { points, loaded, updateValue, inputValue, dateValue } = this.state;
    const data = this.getChartData(points);
    return (
      <React.Fragment>
        <div className="container h-100">
          <div className="row h-100">
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3 h-100">
              <div style={{ padding: '20px' }}>
                <Form dateValue={dateValue} inputValue={Number(inputValue)} updateValue={Boolean(updateValue)} handleInputChange={this.handleInputChange} handleDateChange={this.handleDateChange} handleSubmit={this.handleSubmit} />
              </div>
            </div>
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-9 h-100">
              {loaded && <Chart data={data}/>}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
