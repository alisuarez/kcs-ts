export const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;
export const FETCH_POINTS_URL = `${API_BASE_URL}/data`;
export const ADD_POINTS_URL = `${API_BASE_URL}/points`;
